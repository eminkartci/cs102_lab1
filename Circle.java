public class Circle {

    // Attributes

        // x - y for loaction -> double
        // radus for radius   -> double
    public double x;
    public double y;
    public double radius;

    // =========> Constructor <=========

    // Constructor 1
        // Gets all values
    public Circle(double x,double y,double radius){
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    // Constructor 2
        // Initializes a unit circle
    public Circle(){
        this.x = 0;
        this.y = 0;
        this.radius = 1;
    }


    // =========> Behaviours <=========
    
    // Calculate the are by the formula
        // Area = pi * r^2
    public double getArea(){
        // return directly
        return Math.PI * Math.pow(this.radius,2);
    }

    // Translate the circrle by:
        // dx => dx amount of among x axis
        // dy => dy amount of among y axis
    public void translate(double dx,double dy){
        // Translate the coordinates
        this.x += dx;
        this.y += dy;
    }

    // Scale the circle
        // multiply the radius by scaler
        // assign new value to the object
    public void scale(double scaler){
        this.radius *= scaler;
    }

    // Print the circle's information to the screen
    public String print_circle(boolean willPrint){
        // Create a string format to print
        String circleInfo = "\n--------- Circle ---------"+
                            "\nX      : " + this.x+
                            "\nY      : " + this.y+
                            "\nRadius : " + this.radius+
                            "\n--------------------------\n";

        // if the user wants to print to the console print it
        if(willPrint){
            System.out.println(circleInfo);
        }
        // return the string so that we can user the string other places
        return circleInfo;

    }

    // =========> GETTER - SETTER <=========

    public double getX(){
        return this.x;
    }

    public void setX(double x){
        this.x = x;
    }

    public double getY(){
        return this.y;
    }

    public void setY(double y){
        this.y = y;
    }

    public double getRadius(){
        return this.radius;
    }

    public void setRadius(double radius){
        this.radius = radius;
    }

    
}