
public class Lab1 {

    public static void main(String[] args){

        // Create a new circle instance
        Circle myCirc = new Circle(-2.06,-0.23,1.33);

        // State 1
        myCirc.print_circle(true);

        // Actions
        myCirc.translate(2.06, 5.23);
        myCirc.scale(1.88/1.33);

        // State 2
        myCirc.print_circle(true);

        // Actions
        myCirc.translate(5, 0);

        // State 3
        myCirc.print_circle(true);

        // Actions
        myCirc.scale(0.94/1.88);

        // State 4
        myCirc.print_circle(true);

        // Actions
        myCirc.translate(-1, -1);
        myCirc.scale(3.1/0.94);

        // State 5
        myCirc.print_circle(true);

    }

}